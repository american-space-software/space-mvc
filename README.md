# Space MVC

<p align="center">
  <img src="logo.png" width="200" />
</p>

A TypeScript toolset to simplify building peer-to-peer smart contract applications using Ethereum, IPFS, and OrbitDB. All the tools you'll need to get started quickly. Smart contracts, storage, MVC, and unit tests with a proven MVC pattern inspired by proven enterprise Java/.NET libraries like Spring.

SpaceMVC makes it easy to build professional mobile, desktop, web, and Node applications using next generation internet technologies. 

## Our primary goals
* Provide a radically faster getting started experience for P2P app development.
* Make opinionated decisions to reduce boilerplate code.
* Implements simple, familiar MVC pattern. 
* Mobile, desktop, web, and Node applications from a single code base. 
* Test Ethereum smart contracts with Truffle. 
* Mocha unit tests for business logic and full integration tests. 
* Lightweight. Less than 5MB total.
* Use dependency injection to modularize your code. 
* Apps work offline by default. Data is primarily stored in the user's browser. 

SpaceMVC provides a pre-configured development environment with reasonable defaults. Start building real apps immediately. 

### Maintainable and Testable
* Object-oriented design and [Inversify](http://inversify.io/) dependency injection helps you decouple your business logic and your UI to write maintainable and testable code.  
* SpaceMVC relies heavily on [Framework7](https://framework7.io/) and the answers to many questions can be found in their excellent documentation. You can use it to build full featured iOS, Android & Desktop apps that look and feel native. It uses nothing but HTML, CSS, and Javascript.

### Important Technology 
* [Typescript](https://www.typescriptlang.org/) - TypeScript is a typed superset of JavaScript that compiles to plain JavaScript.
* [Framework7](https://framework7.io/) - Build full featured iOS, Android & Desktop apps with HTML, CSS, and Javascript
* [IPFS](https://ipfs.io) - A peer-to-peer hypermedia protocol
designed to make the web faster, safer, and more open.
* [OrbitDB](https://github.com/orbitdb/orbit-db) - OrbitDB is a serverless, distributed, peer-to-peer database
* [The Ethers Project](https://github.com/ethers-io/ethers.js) - A complete Ethereum wallet implementation and utilities in JavaScript (and TypeScript).
* [Inversify](http://inversify.io/) - IoC container Inversify.

## Getting Started 

The fastest way to create a new project is to clone the [SpaceMVC Starter](https://gitlab.com/american-space-software/space-mvc-starter) project. It's an example of a basic CRUD application that saves and loads lists of students for a class roster.  

For a more complete example see [American Sports Oligarch](https://gitlab.com/american-space-software/american-sports-oligarch).


## Example

```typescript

    import "core-js/stable"
    import "regenerator-runtime/runtime"

    import SpaceMVC from "space-mvc";

    // Set up Inversify container with dependencies.

    //Example bindings. See iversify docs for full details. You'll bind your controllers, services, and DAOs here.
    import { FranchiseService } from "./service/franchise-service" //just an example of a service
    import { LeagueController } from "./controller/league-controller" //just ann example of a controller.

    const container = new Container()
    container.bind(FranchiseService).toSelf().inSingletonScope()
    container.bind(HomeController).toSelf().inSingletonScope()


    /**
     *  Create Framework 7 init params.
     * 
     *  See Framework7 documentation for available options: https://framework7.io/docs/app.html
     * 
     *  Please note that the routes field is optional. 
     *  SpaceMVC maps routes with controller annotations.
     *  Passing a value will disable that mechanism.
     * 
    */ 
    let f7Config = {
        root: '#app', // App root element
        id: 'example', // App bundle ID
        name: 'SpaceMVC Example', // App name
        theme: 'auto', // Automatic theme detection
    }

    /**
     * Configure the Orbit DB store definitions that we'll open in the WALLET_METASTORE (see below)
     * For each StoreDefinition it will open an OrbitDB store. 
     */ 

    let storeDefinitions:StoreDefinition[] = [{
        name: "settings",
        type: "kvstore",
        load: 100,
        options: {}
    }]

    /*
     * Using a truffle contract as an example. 
     
       Contracts are available by injecting the ContractService into your services or DAOs. 
       Call getContract(name) to get the initialized ethers contract. 

    */
    let truffleJson = require('../truffle/build/contracts/YourContract.json')
    let contract:Contract = await ContractService.createContractFromTruffle(truffleJson)


    /**
     * By default the app will connect the "homestead" ethers provider. 
     * You can override this by passing a value for defaultProvider
     */ 

    let spaceMvcInit:SpaceMvcInit = {
        name: "demo-project",
        displayName: "Demo Project",
        container: container,
        f7Config: f7Config,
        storeDefinitions: storeDefinitions,
        contracts: [contract],
        defaultProvider: new ethers.providers.JsonRpcProvider("http://localhost:7545"), //an example to override and connect to ganache 
    }

    await SpaceMVC.init(spaceMvcInit)

```

## Dependency Injection
Use [Inversify](https://inversify.io/) to configure and inject dependencies into our services, DAOs, controllers, and more. You can also extend this behavior and bind your own objects to the container. You can also inject the services that SpaceMVC exports into your own classes. 

The inversify container can be accessed at
```typescript
SpaceMVC.getContainer()
```

## MVC
SpaceMVC implements a Model-View-Controller pattern.

## Controllers
URL routes are mapped to controller functions with the @routeMap() annotation. 

1. Bind your controller in singleton scope with Inversify
2. Routes are mapped by the @routeMap() annotation. Each annotation must have a globally unique path. When that URL is accessed the method it's mapped to will be called.
3. The controller method returns a ModelView object. It returns the model to render and the view is a Framework7 component with a template.
4. SpaceMVC creates the component and passes the model data to it. 
5. See [Framework7 documentation](https://framework7.io/docs/router-component.html) for on-page event options.

## Services
Services are where your business logic goes. A service is a POJO annotated with the @service() annotation. 

1. Bind service in singleton scope with inversify.
2. Create methods and associated unit tests. 
3. Inject ContractService to make calls to smart contracts.
4. Inject your data access objects (DAOs) to store data in OrbitDB or IPFS.


## DAOs
Data Access Objects (DAOs) store and retreive data from OrbitDB, MFdb and IPFS. A DAO is a POJO with a @dao() annotation. 

1. Bind DAO in singleton scope with inversify.
2. Create methods and associated unit tests.
3. Inject DAO into service to store and retrieve data. 


## Importing Truffle Contract

SpaceMVC uses ethers.js under the hood for contract communication but we also use Truffle to develop Solidity unit test suites. 

We have a basic interface that's agnostic to implementation but an easy way to import Truffle JSON files. 

```typescript
interface Contract {
    name:string,
    networks:Network[],
    abi:any
}

interface Network {
    networkId:string, //network ID is exposed by provider.send("net_version")
    address:string //the address where the contract is deployed.
}
```

Example: This will grab all the networking information out of the Truffle JSON and into the right format.

```javascript
import { ContractService } from "space-mvc"

let truffleJson = require('../truffle/build/contracts/YourContract.json')

let contract:Contract = await ContractService.createContractFromTruffle(truffleJson)

```


## Metastore Service
A Metastore is an object that loads and connects to a group of OrbitDB stores. This allows you to define all of the stores that your application needs and open them all at once. 

A Metastore named "WALLET_METASTORE" is created by default.  It's only writeable by the user's wallet address. The app will open and load all of the stores and OrbitDB will sync all of the stores to all of the user's devices. 

*Interface*
```typescript 
    interface Metastore {
        name:string
        writeAccess?: string[]
        storeDefinitions?:StoreDefinition[]

        addresses?: any
        stores?: any
    }
```

### Store Definition

A store definition contains info to open an [OrbitDB](https://github.com/orbitdb/orbit-db).

* Name - the name of the store
* Type - the type of Orbit store to open. Options are log, feed, keyvalue, docs, counter, and mfsstore
* Load - the number of records to load into memory when the store is loaded
* Options - options that will be passed to store initialization


*Creating a custom Metastore* 
```typescript

    //Configure a metastore a name, an array of keys with write access to the stores and the store definitions.
    let metastore = {
        name: "NBA_STATS",
        writeAccess: ["0x3Cf6079639D48D37b741D4bcF7c04f2C02062C46"],
        storeDefinitions:  [
            {
                name: "player",
                type: "mfsstore",
                load: 100,
                options: {
                    schema: {
                        playerKey: { unique: true },
                        firstName: { unique: false },
                        lastName: { unique: false }
                    }
                }
            }
        ]
    }

    //Connect
    await metastoreService.connect(metastore)
```

### Loading Stores
```TypeScript
    //To load a store later
    await metastoreService.getStore("YOUR METASTORE NAME", "THE ORBITDB STORE NAME IN STORE DEFINITION")

    //Load a store from the default wallet metastore
    let store = metastoreService.getWalletStore("student")
```


## Events

* spacemvc:initFinish - Fired when SpaceMVC has finished initializing.

```typescript
    //Create a listener
    SpaceMVC.getEventEmitter().on('spacemvc:initFinish', async () => {
        //Do stuff.
    })
```

# Build

Production:

```console
npm run build
```

Development:

```console
npm run build:dev
```

Tests:

```console
npm run test
```


* dist/index-browser.js - Browser build
* dist/index-node.js - Node build




