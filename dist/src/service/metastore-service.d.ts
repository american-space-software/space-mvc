import { OrbitService } from "./orbit-service";
import { Metastore } from "../dto/metastore";
declare class MetastoreService {
    private orbitService;
    WALLET_METASTORE: string;
    private cache;
    constructor(orbitService: OrbitService);
    /**
     * Connects and caches the passed in metastore
     * @param metastore
     */
    connect(metastore: Metastore): Promise<void>;
    /**
     * Passed in metastore object is populated with store addresses based on the owner and storeDefinitions inside
     * of it.
     * @param metastore
     */
    loadStoreAddresses(metastore: Metastore): Promise<void>;
    /**
     * Takes a Metastore object and then loads all the orbit stores associated with it.
     *
     * Reads the storeDefinitions from the schema and then calls load() on the orbit store.
     *
     * Returns the LoadedMetastore
     *
     * @param metastore
     */
    loadStores(metastore: Metastore): Promise<void>;
    /**
     * Adds passed Metastore to the cache.
     * @param metastore
     */
    cacheMetastore(metastore: Metastore): void;
    /**
     * Returns a store by name.
     * @param storeName
     */
    getStore(metastoreName: string, storeName: string): any;
    getWalletStore(storeName: string): any;
    /**
     * Drops the store.
     *
     * @param metastoreName
     * @param storeName
     */
    dropStore(metastoreName: string, storeName: string): Promise<any>;
    /**
     * This is view logic and should probably move.
     */
    getWalletMetastoreInfos(): Promise<MetastoreInfo[]>;
    getMetastoreInfo(metastore: Metastore): Promise<MetastoreInfo>;
    /**
     * Builds default options for a store
     * @param ownerAddress
     */
    private getDefaultOptions;
    /**
     * Returns the name of a store built from the parameters that get passed.
     * @param store
     * @param ownerAddress
     */
    private createStoreName;
    /**
     * Create an orbitdb store based on the definition
     * @param store
     * @param storeName
     * @param options
     */
    private createStore;
}
interface MetastoreInfo {
    name: string;
    stores: any[];
    writeAccess: string[];
}
export { MetastoreService, MetastoreInfo };
