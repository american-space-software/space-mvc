import { WalletDao } from "../dao/wallet-dao";
import { UiService } from "./ui-service";
declare class WalletService {
    private walletDao;
    private uiService;
    provider: any;
    wallet: any;
    constructor(walletDao: WalletDao, uiService: UiService);
    init(defaultProvider?: any): Promise<void>;
    createWallet(password: any): Promise<string>;
    connectWallet(wallet: any): Promise<void>;
    unlockWallet(password: string): Promise<void>;
    restoreWallet(recoverySeed: string, password: string): Promise<any>;
    getWallet(): Promise<any>;
    getAddress(): Promise<any>;
    logout(): void;
}
export { WalletService };
