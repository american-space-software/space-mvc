import { IdentityService } from "./identity-service";
import { IpfsService } from "./ipfs-service";
declare class OrbitService {
    private ipfsService;
    private identityService;
    private orbitDbOptions;
    orbitDb: any;
    constructor(ipfsService: IpfsService, identityService: IdentityService, orbitDbOptions: any);
    init(): Promise<void>;
    initIdentity(wallet: any): Promise<void>;
}
export { OrbitService };
