declare class WalletDao {
    private name;
    db: any;
    constructor(name: string);
    saveWallet(wallet: any): Promise<any>;
    loadWallet(): Promise<any>;
}
export { WalletDao };
