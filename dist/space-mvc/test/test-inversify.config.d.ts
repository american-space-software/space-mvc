import "reflect-metadata";
import { Container } from "inversify";
import { Contract } from "../src/dto/spacemvc-init";
import { StoreDefinition } from "../src/dto/store-definition";
export declare function initTestContainer(container: Container, provider: any, wallet: any, storeDefinitions: StoreDefinition[], contracts: Contract[]): Promise<Container>;
export declare function getTestContainer(): Promise<Container>;
