declare function routeMap(value: string, showSpinner?: boolean): (target: any, propertyKey: string, descriptor: PropertyDescriptor) => void;
export { routeMap };
