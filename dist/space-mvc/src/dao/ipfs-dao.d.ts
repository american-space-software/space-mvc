import { IpfsService } from "../service/ipfs-service";
declare class IpfsDAO {
    private ipfsService;
    constructor(ipfsService: IpfsService);
    put<T extends IpfsDTO>(dto: T, options?: any): Promise<T>;
    load<T extends IpfsDTO>(dto: T, options?: any): Promise<void>;
}
interface IpfsDTO {
    ipfsCid?: string;
}
export { IpfsDAO, IpfsDTO };
