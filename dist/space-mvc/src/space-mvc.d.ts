/// <reference types="node" />
import { EventEmitter } from "events";
import { StoreDefinition } from "./dto/store-definition";
import { Container } from "inversify";
import { SpaceMvcInit } from "./dto/spacemvc-init";
export declare namespace SpaceMVC {
    function app(): any;
    function name(): string;
    function displayName(): string;
    function getEventEmitter(): EventEmitter;
    function getContainer(): Container;
    function init(spaceMvcInit: SpaceMvcInit): Promise<void>;
    function initFinish(storeDefinitions: StoreDefinition[]): Promise<void>;
}
