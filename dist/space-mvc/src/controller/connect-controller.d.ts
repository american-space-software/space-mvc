import { ConnectService } from "../service/connect-service";
import { ModelView } from "../util/model-view";
import { MetastoreService } from "../service/metastore-service";
declare class ConnectController {
    private connectService;
    private metastoreService;
    constructor(connectService: ConnectService, metastoreService: MetastoreService);
    index(): Promise<ModelView>;
}
export { ConnectController };
