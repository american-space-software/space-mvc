import { WalletService } from "../service/wallet-service";
import { UiService } from "../service/ui-service";
import { ModelView } from "../util/model-view";
import { RoutingService } from "../service/routing-service";
import { StoreDefinition } from "../dto/store-definition";
import { ContractService } from "../service/contract-service";
declare class WalletController {
    private walletService;
    private contractService;
    private uiService;
    private routingService;
    private storeDefinitions;
    constructor(walletService: WalletService, contractService: ContractService, uiService: UiService, routingService: RoutingService, storeDefinitions: StoreDefinition[]);
    private _initApp;
    showLanding(): Promise<ModelView>;
    showCreateWallet(): Promise<ModelView>;
    createWallet(): Promise<ModelView>;
    createWalletDone(): Promise<ModelView>;
    unlockWallet(): Promise<ModelView>;
    showEnterRecovery(): Promise<ModelView>;
    restoreButtonClick(): Promise<ModelView>;
}
export { WalletController };
