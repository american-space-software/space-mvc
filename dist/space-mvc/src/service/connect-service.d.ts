import { IpfsService } from "./ipfs-service";
import { ConnectInfo } from "../dto/connect-info";
declare class ConnectService {
    private ipfsService;
    constructor(ipfsService: IpfsService);
    getConnectInfo(): Promise<ConnectInfo>;
    addPeer(peerAddress: string): Promise<any>;
}
export { ConnectService };
