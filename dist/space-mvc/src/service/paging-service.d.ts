declare class PagingService {
    constructor();
    /**
     * This function takes in a limit, offset, and count and generates a model
     *
     * @param offset
     * @param limit
     * @param count
     */
    buildPagingViewModel(offset: number, limit: number, count: number): PagingViewModel;
    validateLimitOffset(limit: any, offset: any, currentCount: any): void;
    calculateEndIndex(limit: any, offset: any, currentCount: any): number;
    calculateDescendingEndIndex(limit: any, offset: any): number;
    calculateDescendingOffset(offset: any, currentCount: any): number;
}
declare class PagingViewModel {
    offset: number;
    limit: number;
    count: number;
    start: number;
    end: number;
    previousOffset: number;
    nextOffset: number;
}
export { PagingService, PagingViewModel };
