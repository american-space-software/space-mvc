import { Container } from "inversify";
import { UiService } from "./ui-service";
import { Router } from "framework7/modules/router/router";
declare class RoutingService {
    private uiService;
    app: any;
    constructor(uiService: UiService, app: any);
    navigate(navigateParams: Router.NavigateParameters, routeOptions?: Router.RouteOptions, viewName?: string): void;
    navigateUrl(url: string, routeOptions?: Router.RouteOptions, viewName?: string): void;
    buildRoutesForContainer(existing: any, container: Container): Router.RouteParameters[];
    submitForm(e: Event, formId: string, $: any): void;
    private resolveRoute;
}
interface RouteTo {
    context: any;
    params: any;
    url: string;
    path: string;
    query: any;
    name: string;
    hash: string;
    route: any;
}
interface Route {
    path: string;
    method: string;
}
export { RoutingService, Route, RouteTo };
