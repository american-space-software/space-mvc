declare class IpfsService {
    private ipfsOptions;
    ipfs: any;
    constructor(ipfsOptions: any);
    init(): Promise<void>;
}
export { IpfsService };
