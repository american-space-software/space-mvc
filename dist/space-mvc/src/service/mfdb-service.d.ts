import { MFdb } from "mfdb";
import { IpfsService } from "./ipfs-service";
declare class MfdbService {
    private ipfsService;
    mfdb: MFdb;
    constructor(ipfsService: IpfsService);
    init(): Promise<void>;
}
export { MfdbService };
