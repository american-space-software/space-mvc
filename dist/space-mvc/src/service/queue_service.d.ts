import { PromiseView } from "../util/promise-view";
declare class QueueService {
    private app;
    constructor(app: any);
    queuePromiseView(promiseView: PromiseView): Promise<any>;
    private _beforeSaveAction;
    private _showError;
    private _showSuccess;
}
interface QueueItem {
    toast?: any;
    id: string;
    icon: string;
    title: string;
}
export { QueueService, QueueItem };
