import { ethers } from "ethers";
import { Contract } from "../dto/spacemvc-init";
import { WalletService } from "./wallet-service";
declare class ContractService {
    private walletService;
    private contracts;
    ethersContracts: any;
    constructor(walletService: WalletService, contracts: Contract[]);
    init(): Promise<void>;
    getContract(name: string): ethers.Contract;
    static createContractFromTruffle(truffleJson: any): Promise<Contract>;
}
export { ContractService };
