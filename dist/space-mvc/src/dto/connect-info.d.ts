interface ConnectInfo {
    peers: any;
    ipfsInfo: any;
    peerCount: number;
    subscribed: any;
}
export { ConnectInfo };
