import dao from "../decorators/dao"
import { IpfsService } from "../service/ipfs-service"

const all = require('it-all')
const uint8ArrayFromString = require('uint8arrays/from-string')
const uint8ArrayToString = require('uint8arrays/to-string')
const uint8ArrayConcat = require('uint8arrays/concat')

@dao()
class IpfsDAO {

    constructor(
        private ipfsService:IpfsService
    ) {}

    async put<T extends IpfsDTO>(dto:T, options?:any) : Promise<T> {
        
        //Do not save the ipfsCid if it's passed in.
        delete dto.ipfsCid

        const result = await this.ipfsService.ipfs.add({
            content: JSON.stringify(dto)
        }, options)

        dto.ipfsCid = result.cid.toString()
        
        return dto 
    }

    async load<T extends IpfsDTO>(dto:T, options?:any) {

        const files = await all(this.ipfsService.ipfs.get(dto.ipfsCid, options))

        let metadata = JSON.parse(uint8ArrayToString(uint8ArrayConcat(await all(files[0].content))))

        metadata.ipfsCid = dto.ipfsCid
        
        Object.assign(dto, metadata)

    }

}

interface IpfsDTO {
    ipfsCid?:string
}



export {
    IpfsDAO, IpfsDTO
}