import { Contract } from "ethers"
import { inject, injectable } from "inversify"

const levelup = require('levelup')
const leveljs = require('level-js')


@injectable()
class WalletDao {

  db: any

  constructor(
    @inject("name") private name:string
  ) {
    this.db = levelup(leveljs(`${this.name}-wallet`)) //TODO: Make sure this name can be customized per app
  }


  async saveWallet(wallet) {
    return this.db.put('wallet', Buffer.from(JSON.stringify(wallet)))
  }

  async loadWallet() {

    try {
      const value = await this.db.get('wallet')

      if (value) {
        return JSON.parse(value.toString())
      }

    } catch (ex) {
      // console.log(ex)
    }

  }




}

export {
  WalletDao
}