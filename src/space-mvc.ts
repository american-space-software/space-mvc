import { UiService } from "./service/ui-service"
import { PagingService } from "./service/paging-service"
import { IdentityService } from "./service/identity-service"
import { WalletService } from "./service/wallet-service"
import { EventEmitter } from "events"
import { MetastoreService } from "./service/metastore-service"
import { StoreDefinition } from "./dto/store-definition"
import { Container } from "inversify"

import { OrbitService } from "./service/orbit-service"
import { IpfsService } from "./service/ipfs-service"
import { InvalidWalletException } from "./service/exception/invalid-wallet-exception"
import { buildContainer } from "./inversify.config"
import { RoutingService } from "./service/routing-service"
import { QueueService } from "./service/queue_service"
import { Metastore } from "./dto/metastore"
import { Contract, SpaceMvcInit } from "./dto/spacemvc-init"
import { ContractService } from "./service/contract-service"

const eventEmitter: EventEmitter = new EventEmitter()
let spaceCommand:SpaceCommand

export namespace SpaceMVC {

    export function app() {
        return spaceCommand.app
    }

    export function name() {
        return spaceCommand.name
    }

    export function displayName() {
        return spaceCommand.displayName
    }

    export function getEventEmitter() {
        return eventEmitter
    }

    export function getContainer() : Container {
        return spaceCommand.container
    }

    export async function init(spaceMvcInit:SpaceMvcInit) {
        spaceCommand = new SpaceCommand()
        return spaceCommand.init(spaceMvcInit)
    }

    export async function initFinish(storeDefinitions: StoreDefinition[]) {
        return spaceCommand.initFinish(storeDefinitions)
    }

}

class SpaceCommand {

    public container:Container

    public name: string
    public displayName: string 

    async init(spaceMvcInit:SpaceMvcInit) {

        if (!spaceMvcInit.name) {
            throw new Error("No application name supplied.")
        }

        this.name = spaceMvcInit.name
        this.displayName = spaceMvcInit.displayName

        spaceMvcInit.container.bind('storeDefinitions').toConstantValue(spaceMvcInit.storeDefinitions)
        spaceMvcInit.container.bind("contracts").toConstantValue(spaceMvcInit.contracts)
        spaceMvcInit.container.bind("name").toConstantValue(spaceMvcInit.name)

        //Initialize container
        await this.initContainer(spaceMvcInit.container, spaceMvcInit.f7Config, this.name)
        
        //Initialize wallet
        await this.walletService.init(spaceMvcInit.defaultProvider)

        //Initialize contracts
        await this.contractService.init()

        //Initialize IPFS
        await this.ipfsService.init()

        //Initialize routing
        await this.initRouting()

        //Did we get a wallet already?
        if (this.walletService.wallet) {

            this.initMainView()
            await this.initFinish(spaceMvcInit.storeDefinitions)   

        } else {

            this.initMainView()
            this.routingService.navigate({
                path: "/wallet"
            })

        }
    }


    initContainer(container: Container, f7Config, name:string) {
        console.log("Initializing container")
        this.container = buildContainer(container, f7Config, name) 
    }    

    initRouting() {

        console.log("Initializing routing")

        //Include default routing
        let routes = this.routingService.buildRoutesForContainer(this.app.routes, this.container)
        this.app.routes = routes

    }

    initMainView() {

        //Create main view
        this.app.views.create('.view-main', {
            browserHistory: true,
            loadInitialPage: false
        })
    }

    async initWalletMetastore(storeDefinitions: StoreDefinition[]) {

        if (!this.walletService.wallet) {
            throw new InvalidWalletException("No wallet found. Unable to initialize.")
        }

        let walletAddress = await this.walletService.wallet.getAddress()

        console.log(`Initializing ${this.name} for wallet: ${walletAddress}`)

        this.uiService.showSpinner('Initializing wallet')

        await this.orbitService.init()

        // Open the schema associated with the wallet and cache it.
        let metastore:Metastore = {
            name: this.metastoreService.WALLET_METASTORE,
            writeAccess: [await this.walletService.wallet.getAddress()],
            storeDefinitions: storeDefinitions
        }

        await this.metastoreService.connect(metastore)

        this.uiService.hideSpinner()
    }

    async initFinish(storeDefinitions: StoreDefinition[]) {

        await this.initWalletMetastore(storeDefinitions)    
        
        this.routingService.navigate({
            path: "/"
        })

        eventEmitter.emit('spacemvc:initFinish')

    }


    public get ipfsService(): IpfsService {
        return this.container.get<IpfsService>(IpfsService)
    }

    public get orbitService(): OrbitService {
        return this.container.get<OrbitService>(OrbitService)
    }

    public get queueService(): QueueService {
        return this.container.get<QueueService>(QueueService)
    }

    public get pagingService(): PagingService {
        return this.container.get<PagingService>(PagingService)
    }

    public get uiService(): UiService {
        return this.container.get<UiService>(UiService)
    }

    public get identityService(): IdentityService {
        return this.container.get<IdentityService>(IdentityService)
    }

    public get walletService(): WalletService {
        return this.container.get<WalletService>(WalletService)
    }

    public get contractService(): ContractService {
        return this.container.get<ContractService>(ContractService)
    }

    public get metastoreService(): MetastoreService {
        return this.container.get<MetastoreService>(MetastoreService)
    }

    public get routingService(): RoutingService {
        return this.container.get<RoutingService>(RoutingService)
    }

    public get app(): any {
        return this.container.get("framework7")
    }

}

