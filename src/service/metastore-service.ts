import { OrbitService } from "./orbit-service";
import service from "../decorators/service";
import { Metastore } from "../dto/metastore";
import { StoreDefinition } from "../dto/store-definition";



@service()
class MetastoreService {

    public WALLET_METASTORE = "WALLET_METASTORE"

    private cache: {
        [key:string]:Metastore
    } = {}

    constructor(
        private orbitService:OrbitService
    ) {}


    /**
     * Connects and caches the passed in metastore
     * @param metastore 
     */
    async connect(metastore:Metastore) {

        await this.loadStoreAddresses(metastore)
        await this.loadStores(metastore)
        await this.cacheMetastore(metastore)

    }

    /**
     * Passed in metastore object is populated with store addresses based on the owner and storeDefinitions inside 
     * of it.
     * @param metastore 
     */
    async loadStoreAddresses(metastore:Metastore) {

        metastore.addresses = []

        let defaultOptions = this.getDefaultOptions(metastore.writeAccess)

        if (metastore.storeDefinitions) {
            for (let store of metastore.storeDefinitions) {

                let options = {
                    ...defaultOptions,
                    ...store.options
                }
                let storeName = this.createStoreName(store, metastore.writeAccess)

                let createdStore = await this.createStore(store, storeName, options)
    
                metastore.addresses[store.name] = createdStore.address.toString()
    
            }
        }

    }


    /**
     * Takes a Metastore object and then loads all the orbit stores associated with it. 
     * 
     * Reads the storeDefinitions from the schema and then calls load() on the orbit store.
     * 
     * Returns the LoadedMetastore
     * 
     * @param metastore 
     */
    async loadStores(metastore: Metastore) {

        metastore.stores = []

        for (let field in metastore.addresses) {

            //Get definition
            let definition = metastore.storeDefinitions.filter(def => def.name == field)[0]

            //Open the store
            metastore.stores[field] = await this.orbitService.orbitDb.open(metastore.addresses[field])

            //Load if configured
            if (definition.load) {
                console.log(`Loading ${field}`)

                await metastore.stores[field].load(definition.load)
            } else {
                console.log(`Skipping ${field}`)
            }
        }

    }

    /**
     * Adds passed Metastore to the cache. 
     * @param metastore 
     */
    cacheMetastore(metastore:Metastore) {
        this.cache[metastore.name] = metastore
    }




    /**
     * Returns a store by name. 
     * @param storeName 
     */
    getStore(metastoreName:string, storeName: string) {
        return this.cache[metastoreName].stores[storeName]
    }

    getWalletStore(storeName:string) {
        return this.getStore(this.WALLET_METASTORE, storeName)
    }


    /**
     * Drops the store. 
     * 
     * @param metastoreName 
     * @param storeName 
     */
    async dropStore(metastoreName:string, storeName:string) {
        return this.getStore(metastoreName, storeName).drop()
    }

    /**
     * This is view logic and should probably move. 
     */
    async getWalletMetastoreInfos() : Promise<MetastoreInfo[]> {

        let metastoreInfos:MetastoreInfo[] = []

        for (let storeName in this.cache) {
            metastoreInfos.push(await this.getMetastoreInfo(this.cache[storeName]))
        }

        return metastoreInfos

    }


    async getMetastoreInfo(metastore:Metastore) : Promise<MetastoreInfo> {

        let stores = metastore.stores

        let translated = []
        for (let key in stores) {
            
            let store = stores[key]
            let count = await store.count()

            translated.push({
                name: key,
                address: store.address.toString(),
                loaded: count
            })
        }

        return {
            stores: translated,
            name: metastore.name,
            writeAccess: metastore.writeAccess
        }
    }
    
    /**
     * Builds default options for a store
     * @param ownerAddress 
     */
    private getDefaultOptions(writeAccess: string[]) {

        return {
            create: true,
            accessController: {
                write: writeAccess
            }
        }
        
    }

    /**
     * Returns the name of a store built from the parameters that get passed. 
     * @param store
     * @param ownerAddress 
     */
    private createStoreName(store: StoreDefinition, writeAccess: string[]) : string {
        return `${store.name}-${writeAccess.toString()}`
    }

    /**
     * Create an orbitdb store based on the definition 
     * @param store 
     * @param storeName 
     * @param options 
     */
    private async createStore(store: StoreDefinition, storeName: string, options:any) {
        
        switch (store.type) {
            case "kvstore":
                return this.orbitService.orbitDb.kvstore(storeName, options)

            case "docs":
                return this.orbitService.orbitDb.docs(storeName, options)

        }

    }
    

}

interface MetastoreInfo {
    name:string 
    stores:any[]
    writeAccess:string[]
}


export {
    MetastoreService, MetastoreInfo
}
