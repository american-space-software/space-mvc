import { injectable, inject } from "inversify";

import service from "../decorators/service";


@service()
class UiService {

    constructor(@inject("framework7") public app) {
    }

    showExceptionPopup(ex) {
        console.log(ex)
        this.app.dialog.alert(ex.message, "There was an error")
    }

    showPopup(message) {
        this.app.dialog.alert(message)
    }


    /**
     * Spinner
     */

    spinnerDialog: any

    showSpinner(message?:string) {

        if (this.spinnerDialog) this.hideSpinner()

        this.spinnerDialog = this.app.dialog.preloader(message ? message : "Loading")
    }


    hideSpinner() {
        if (this.spinnerDialog) {
            this.spinnerDialog.close()
            this.spinnerDialog = null 
        }
    }
}

export {
    UiService
}