const { ethers, Wallet, providers } = require('ethers')
import { WalletDao } from "../dao/wallet-dao";
import service from "../decorators/service";
import { UiService } from "./ui-service";

@service()
class WalletService {

  public provider: any
  public wallet: any


  constructor(
    private walletDao: WalletDao,
    private uiService: UiService
  ) {}

  async init(defaultProvider?) {
    
    console.log("Initializing wallet")

    if (typeof window !== "undefined" && window['ethereum']) {

      // Request account access
      await window['ethereum'].enable()

      //@ts-ignore
      window.web3Provider = window.ethereum

      //@ts-ignore
      // web3 = new Web3(window.web3Provider)

      //@ts-ignore
      this.provider = new providers.Web3Provider(web3.currentProvider)
      this.wallet = this.provider.getSigner()

    } else {

      //We need our own provider.
      if (defaultProvider) {
        this.provider = defaultProvider
      } else {
        this.provider = ethers.getDefaultProvider("homestead")
      }

    }

  }


  async createWallet(password): Promise<string> {

    this.uiService.showSpinner("Creating wallet. Please wait.")

    //Generate random words
    let wallet = await Wallet.createRandom()

    this.uiService.showSpinner("Encrypting wallet...")

    //Encrypt with entered password
    let encryptedJsonWallet = await wallet.encrypt(password)

    await this.walletDao.saveWallet(encryptedJsonWallet)

    await this.connectWallet(wallet)

    this.uiService.hideSpinner()

    return this.wallet.mnemonic.phrase


  }

  async connectWallet(wallet) {
    this.wallet = wallet
    this.wallet = await this.wallet.connect(this.provider)
  }



  async unlockWallet(password: string) {

    let savedWallet = await this.walletDao.loadWallet()

    if (!savedWallet) throw new Error("No wallet to unlock")

    let wallet = await Wallet.fromEncryptedJson(savedWallet, password)

    return this.connectWallet(wallet)

  }

  async restoreWallet(recoverySeed: string, password: string) {

    let wallet = await Wallet.fromMnemonic(recoverySeed)
    if (!wallet) return

    //Encrypt with entered password
    let encryptedJsonWallet = await wallet.encrypt(password)
    await this.walletDao.saveWallet(encryptedJsonWallet)


    await this.connectWallet(wallet)

    return this.wallet

  }

  getWallet() {
    return this.walletDao.loadWallet()
  }

  async getAddress() {
    return this.wallet.getAddress()
  }

  logout() {
    this.wallet = undefined
  }



}



export {
  WalletService
}