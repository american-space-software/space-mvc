
import service from "../decorators/service";

const EthIdentityProvider = require('orbit-db-identity-provider/src/ethereum-identity-provider')
const Identities = require('orbit-db-identity-provider')


@service()
class IdentityService {

    private identity

    constructor(){
        Identities.addIdentityProvider(EthIdentityProvider)
    }
    

    async getIdentity(keystore, wallet) {

        if (this.identity) return this.identity

        const type = EthIdentityProvider.type


        const options = {
            type: type,
            keystore: keystore,
            wallet: wallet
        }

        this.identity = await Identities.createIdentity(options)
        
        return this.identity
    }


    getAccessController(orbitdb) {
        return {
            write: ['*']//[orbitdb.identity.id]
        }
    }


}


export {
    IdentityService
}