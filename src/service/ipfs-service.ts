import { inject, postConstruct } from "inversify";
import service from "../decorators/service";

const IPFS = require('ipfs')

@service()
class IpfsService {

    public ipfs 

    constructor(
        @inject("ipfsOptions") private ipfsOptions
    ){}

    async init() {
        console.log("Initializing IPFS")
        this.ipfs = await IPFS.create(this.ipfsOptions)
    }

}

interface DTO {
    id?:any   
    ipfsCid?:string 
}


export {
    IpfsService
}