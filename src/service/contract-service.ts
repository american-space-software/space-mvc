import { ethers } from "ethers"
import { inject } from "inversify"
import service from "../decorators/service"
import { Contract, Network } from "../dto/spacemvc-init"
import { SpaceMVC } from "../space-mvc"
import { WalletService } from "./wallet-service"

@service()
class ContractService {

    ethersContracts:any = {}

    constructor(    
        private walletService:WalletService,
        @inject("contracts") private contracts:Contract[]
    ) {}

    async init() {

        if (!this.contracts) return 
        if (!this.walletService.provider) return 
        if (!this.walletService.provider.send) return
        if (!this.walletService.wallet) return 

        let networkId = await this.walletService.provider.send("net_version")

        console.log(`Configuring contracts for network ${networkId}`)

        for (let contract of this.contracts) {
            if (contract.networks && contract.networks.length > 0) {
                let matches = contract.networks.filter(val => val.networkId == networkId)
                if (matches[0]) {
                    console.log(`Connecting to ${contract.name} at ${matches[0].address} `)
                    let initContract = new ethers.Contract(matches[0].address, contract.abi, this.walletService.wallet)
                    this.ethersContracts[contract.name] = initContract
                }    
            }
        }

        if (this.contracts.length > 0 && Object.keys(this.ethersContracts).length == 0) {
            console.log("NO CONTRACTS DETECTED ON NETWORK") //consider throwing exception 
        }


    }
    
    getContract(name:string) : ethers.Contract {
        return this.ethersContracts[name]
    }

    static async createContractFromTruffle(truffleJson) : Promise<Contract> {

        let networks:Network[] = []

        for (let network in truffleJson.networks) {
            networks.push({
                networkId: network,
                address: truffleJson.networks[network].address
            })
        }

        return {
            networks: networks,
            abi: truffleJson.abi,
            name: truffleJson.contractName
        }

    }
}

export {
    ContractService
}