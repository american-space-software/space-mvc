import { ConnectService } from "../service/connect-service";
import { routeMap } from "../decorators/route-map";
import { ModelView } from "../util/model-view";

import ConnectIndexComponent from '../components/connect/index.f7.html'
import controller from "../decorators/controller";
import { MetastoreService } from "../service/metastore-service";


@controller()
class ConnectController {

    constructor(
        private connectService:ConnectService,
        private metastoreService:MetastoreService
    ) {}

    @routeMap("/connect")
    async index() {

        return new ModelView( async () => {

            let vm = {
                connect: await this.connectService.getConnectInfo(),
                metastoreInfos: await this.metastoreService.getWalletMetastoreInfos()
            }

            return vm           

        }, ConnectIndexComponent)

    }

}


export {
    ConnectController
}