import { inject } from "inversify"
import { WalletService } from "../service/wallet-service"
import { UiService } from "../service/ui-service"
import { ModelView } from "../util/model-view"

import LandingComponent from '../components/wallet/landing.f7.html'
import EnterRecoveryComponent from '../components/wallet/enter-recovery.f7.html'
import CreateWalletComponent from '../components/wallet/create-wallet.f7.html'
import CreateWalletSuccessComponent from '../components/wallet/create-wallet-success.f7.html'


import { routeMap } from "../decorators/route-map"
import controller from "../decorators/controller"
import { RouteTo, RoutingService } from "../service/routing-service"

import SpaceMVC from ".."
import { StoreDefinition } from "../dto/store-definition"
import { ContractService } from "../service/contract-service"



@controller()
class WalletController {

    constructor(
        private walletService: WalletService,
        private contractService: ContractService,
        private uiService: UiService,
        private routingService: RoutingService,
        @inject("storeDefinitions") private storeDefinitions:StoreDefinition[] 
    ) {
    }

    private async _initApp() {

        this.uiService.showSpinner()

        await this.contractService.init()

        await SpaceMVC.initFinish(this.storeDefinitions)    

        this.uiService.hideSpinner()

    }


    @routeMap("/wallet")
    async showLanding(): Promise<ModelView> {

        return new ModelView(async () => {
            let existingWallet = await this.walletService.getWallet()

            return {
                showUnlock: (existingWallet),
                appName: SpaceMVC.displayName()
            }

        }, LandingComponent)
    }

    @routeMap("/showCreateWallet")
    async showCreateWallet(): Promise<ModelView> {
        return new ModelView(async () => {
        }, CreateWalletComponent)
    }

    @routeMap("/createWallet")
    async createWallet() {

        return new ModelView(async (routeTo:RouteTo) => {
            
            //Create new wallet
            let mnemonic = await this.walletService.createWallet(routeTo.query.newPassword)

            return {
                mnemonic: mnemonic.split(" ")
            }

        }, CreateWalletSuccessComponent)

    }


    @routeMap("/createWalletDone")
    async createWalletDone() {
        return new ModelView(async () => {            
            try {
                await this._initApp()
            } catch (ex) {
                console.log(ex)
                this.uiService.hideSpinner()
                this.uiService.showPopup("Unable to initialize app")
            }
        })
    }



    @routeMap("/unlockWallet")
    async unlockWallet() {
        
        return new ModelView(async (routeTo:RouteTo) => {

            this.uiService.showSpinner('Unlocking wallet...')

            try {
                await this.walletService.unlockWallet(routeTo.query.password)
                await this._initApp()
            } catch (ex) {
                console.log(ex)
                this.uiService.hideSpinner()
                this.uiService.showPopup("Incorrect password please try again.")
            }

        })


    }


    @routeMap("/enterRecovery")
    async showEnterRecovery(): Promise<ModelView> {
        return new ModelView(async () => {
            return {}
        }, EnterRecoveryComponent)

    }

    @routeMap("/restoreAccount")
    async restoreButtonClick() {

        return new ModelView(async (routeTo:RouteTo) => {

            this.uiService.showSpinner('Restoring wallet...')

            let wallet = await this.walletService.restoreWallet(routeTo.query.recoverySeed, routeTo.query.newPassword)

            if (wallet) {
                await this._initApp()
            } else {
                this.uiService.showPopup("Error restoring wallet. Please try again.")
                this.uiService.hideSpinner()
            }

        })

    }

}


export {
    WalletController
}