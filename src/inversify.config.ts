import { Container, interfaces } from "inversify";


import { UiService } from "./service/ui-service";
import { PagingService } from "./service/paging-service";
import { IdentityService } from "./service/identity-service";
import { WalletService } from "./service/wallet-service";
import { MetastoreService } from "./service/metastore-service";

import { OrbitService } from "./service/orbit-service";
import { IpfsService } from "./service/ipfs-service";
import { WalletDao } from "./dao/wallet-dao";
import { WalletController } from "./controller/wallet-controller";
import { RoutingService } from "./service/routing-service";
import { QueueService } from "./service/queue_service";
import { ConnectController } from "./controller/connect-controller";
import { ConnectService } from "./service/connect-service";
import { ContractService } from "./service/contract-service";
import { ContractFactory } from "ethers";
import { IpfsDAO } from "./dao/ipfs-dao";




export function buildContainer(container:Container, config:any, name:string) : Container {

    function framework7() {
        
        const Framework7 = require('framework7/bundle').default


        return new Framework7(config)

    }


    container.bind("framework7").toConstantValue(framework7())

    //Default IPFS options if we're not given any
    if (!container.isBound("ipfsOptions")) {

        container.bind("ipfsOptions").toConstantValue(
            {
                repo: `${name}-repo`,
                EXPERIMENTAL: {
                    pubsub: true
                },
                config: {
                    Addresses: {
                        Swarm: [
                            '/dns4/evening-river-51588.herokuapp.com/tcp/443/wss/p2p-webrtc-star/'
                          ]                    
                        }
                },
                Bootstrap: []
            }
        )
    }

    if (!container.isBound("orbitDbOptions")) {
        container.bind("orbitDbOptions").toConstantValue({})
    }

    container.bind(IdentityService).toSelf().inSingletonScope()
    container.bind(ContractService).toSelf().inSingletonScope()

    container.bind(WalletDao).toSelf().inSingletonScope()
    container.bind(IpfsDAO).toSelf().inSingletonScope()

    container.bind(UiService).toSelf().inSingletonScope()
    container.bind(QueueService).toSelf().inSingletonScope()
    container.bind(PagingService).toSelf().inSingletonScope()
    
    container.bind(MetastoreService).toSelf().inSingletonScope()

    container.bind(OrbitService).toSelf().inSingletonScope()
    container.bind(IpfsService).toSelf().inSingletonScope()
    
    container.bind(WalletService).toSelf().inSingletonScope()
    container.bind(WalletController).toSelf().inSingletonScope()

    container.bind(ConnectService).toSelf().inSingletonScope()
    container.bind(ConnectController).toSelf().inSingletonScope()
    
    container.bind(RoutingService).toSelf().inSingletonScope()

    return container
}