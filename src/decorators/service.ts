import { injectable } from "inversify"

function service(args: any = {}): (cls: any) => any {

    const injectableDecorator = injectable()

    return function (controllerType: any) {
        injectableDecorator(controllerType)
    }
}

export default service