import { injectable } from "inversify"


function controller(): (cls: any) => any {
    const injectableDecorator = injectable()

    return function (controllerType: any) {
        injectableDecorator(controllerType)
    }
}

export default controller