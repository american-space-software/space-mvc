interface StoreDefinition {
    name: string
    type: string
    load?: number
    options: {
        schema?: any
    }
}

export {
    StoreDefinition
}