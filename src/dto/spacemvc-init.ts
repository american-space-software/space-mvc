import { Container } from "inversify";
import { StoreDefinition } from "./store-definition";
import { ethers } from "ethers"

interface SpaceMvcInit {
    name:string,
    displayName: string,
    container:Container, 
    f7Config:any, 
    storeDefinitions?: StoreDefinition[], 
    contracts?:Contract[],
    defaultProvider?:ethers.providers.Provider
    initIpfs?:boolean
    initOrbit?:boolean
}

interface Contract {
    name:string,
    networks:Network[],
    abi:any
}

interface Network {
    networkId:string,
    address:string
}


export {
    SpaceMvcInit,
    Contract,
    Network
}