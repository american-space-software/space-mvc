import { StoreDefinition } from "./store-definition";

/**
 * A Metastore represents a group of orbit stores that are shared together.
 *  
 * The writeAccess and storeDefinitions fields are the input to build the stores.
 * 
 * writeAccess is an array of addresses that have write access to the orbit store.
 * 
 * The addresses field holds the created addresses of each store in the metastore. 
 * 
 * The stores field holds the actual instantiated stores based on the addresses. 
 */
interface Metastore {
    name:string
    writeAccess?: string[]
    storeDefinitions?:StoreDefinition[]

    addresses?: any
    stores?: any
}


export {
    Metastore
}
