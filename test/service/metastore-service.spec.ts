import assert from 'assert'

import { getTestContainer } from "../test-inversify.config"
import { MetastoreService } from '../../src/service/metastore-service'
import { Metastore } from '../../src/dto/metastore'
import { StoreDefinition } from '../../src/dto/store-definition'

let metastore:Metastore


describe('MetastoreService', async () => {

    let metastoreService: MetastoreService

    let storeDefinitions:StoreDefinition[] = [{
        name: "student", 
        type: "kvstore", 
        load: 100, 
        options: {}
    }]
    

    before('Before', async () => {

        let container = await getTestContainer()
        metastoreService = container.get(MetastoreService)
    })    


    it("should populate a metastore's address field", async () => {

        //Arrange
        metastore = {
            name: "themetastore",
            writeAccess: ["ownerAddress"],
            storeDefinitions: storeDefinitions
        }

        //Act
        await metastoreService.loadStoreAddresses(metastore)

        //Assert
        assert.equal(metastore.addresses['student'], '/orbitdb/zdpuAkZYcMweb5YJJgCS5VVPWFFpCb776iXdSpEpuyBfjHww2/student-ownerAddress')
    })

    
    it("should load a metastore's stores from the addresses", async () => {

        //Act
        await metastoreService.loadStores(metastore)

        //Assert
        assert.equal(metastore.addresses['student'], '/orbitdb/zdpuAkZYcMweb5YJJgCS5VVPWFFpCb776iXdSpEpuyBfjHww2/student-ownerAddress')
        assert.equal(metastore.storeDefinitions.length, 1)

        assert.equal(await metastore.stores["student"].address.toString(), '/orbitdb/zdpuAkZYcMweb5YJJgCS5VVPWFFpCb776iXdSpEpuyBfjHww2/student-ownerAddress')


    })

    it("should cache and retreive a metastore", async () => {

        //Act
        metastoreService.cacheMetastore(metastore)

        let store = metastoreService.getStore("themetastore", "student")

        //Assert
        assert.strictEqual(await store.address.toString(), '/orbitdb/zdpuAkZYcMweb5YJJgCS5VVPWFFpCb776iXdSpEpuyBfjHww2/student-ownerAddress' )

    })



})
