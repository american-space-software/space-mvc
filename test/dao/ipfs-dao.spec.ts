import assert from 'assert'

import { getTestContainer } from "../test-inversify.config"
import { IpfsDAO, IpfsDTO } from '../../src/dao/ipfs-dao'


describe('IpfsDAO', async () => {

    let dao: IpfsDAO
    
    before('Before', async () => {

        let container = await getTestContainer()
        dao = container.get(IpfsDAO)
    })    

    it("should put a DTO and retrieve it with get", async () => {

        //Act
        let dto:TestDTO = await dao.put<TestDTO>({
            name: "Test DTO"
        })


        //Assert
        assert.strictEqual(dto.ipfsCid.toString(), "QmTw8uAWLXtg6eEAN23iewWxUiEFVXMtkUwsm9DoJQiECd")
        
        let loadTest:TestDTO = {
            ipfsCid: dto.ipfsCid.toString()
        }
        
        await dao.load<TestDTO>(loadTest)

        assert.strictEqual(loadTest.ipfsCid.toString(), dto.ipfsCid.toString())
        assert.strictEqual(loadTest.name, "Test DTO")

    })


})

interface TestDTO extends IpfsDTO  {
    name?:string
}