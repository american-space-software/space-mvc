import "reflect-metadata"

import { Container } from "inversify";
import { Contract } from "../src/dto/spacemvc-init";
import { IpfsService } from "../src/service/ipfs-service";
import { OrbitService } from "../src/service/orbit-service";
import { IdentityService } from "../src/service/identity-service";
import { StoreDefinition } from "../src/dto/store-definition";
import { MetastoreService } from "../src/service/metastore-service";
import { ContractService } from "../src/service/contract-service";
import { WalletDao } from "../src/dao/wallet-dao";
import { WalletService } from "../src/service/wallet-service";
import { UiService } from "../src/service/ui-service";
import { QueueService } from "../src/service/queue_service";
import { Metastore } from "../src/dto/metastore";
import { IpfsDAO } from "../src/dao/ipfs-dao";
import { PagingService } from "../src/service/paging-service";


let container:Container 

export async function initTestContainer(container: Container, provider, wallet, storeDefinitions:StoreDefinition[], contracts:Contract[]): Promise<Container> {

    //Not needed in tests but need to inject something
    container.bind("framework7").toConstantValue({})

    //Default IPFS options if we're not given any
    if (!container.isBound("ipfsOptions")) {
        container.bind("ipfsOptions").toConstantValue({
            repo: './.tmp/test-repos/test' 
        })
    }

    if (!container.isBound("orbitDbOptions")) {
        container.bind("orbitDbOptions").toConstantValue({
            directory: "./.tmp/orbitdb/" + Math.random().toString()
        })
    }

    container.bind("contracts").toConstantValue(contracts)

    container.bind(IdentityService).toSelf().inSingletonScope()
    container.bind(OrbitService).toSelf().inSingletonScope()
    container.bind(IpfsService).toSelf().inSingletonScope()
    container.bind(MetastoreService).toSelf().inSingletonScope()
    container.bind(ContractService).toSelf().inSingletonScope() 
    container.bind(UiService).toSelf().inSingletonScope() 
    container.bind(WalletService).toSelf().inSingletonScope() 
    container.bind(QueueService).toSelf().inSingletonScope()
    container.bind(PagingService).toSelf().inSingletonScope()

    container.bind(IpfsDAO).toSelf().inSingletonScope()

    //Create a mock wallet DAO. Maybe replace with a version that works in tests. 
    //@ts-ignore
    container.bind(WalletDao).toConstantValue({
        async saveWallet(wallet) {
        },
        async loadWallet() {
        }
    })
    

    //Initialize wallet 
    let walletService:WalletService = container.get(WalletService)
    walletService.provider = provider
    walletService.wallet = wallet


    //Initialize contracts
    let contractService:ContractService = container.get(ContractService)
    await contractService.init()

    //Initialize IPFS
    let ipfsService:IpfsService = container.get(IpfsService)
    await ipfsService.init()

    //Initialize orbit
    let orbitService:OrbitService = container.get(OrbitService)
    await orbitService.initIdentity(wallet)
    
    //Create schema
    let metastoreService:MetastoreService = container.get(MetastoreService)

    let metastore:Metastore = {
        name: metastoreService.WALLET_METASTORE,
        writeAccess: [orbitService.orbitDb.identity.id],
        storeDefinitions: storeDefinitions
    }

    await metastoreService.connect(metastore)
    
    return container
}


export async function getTestContainer() {
    if (container) return container
    
    container = new Container()
    //@ts-ignore
    return initTestContainer(container)

}