const path = require('path')
const nodeExternals = require('webpack-node-externals')
const TypedocWebpackPlugin = require('typedoc-webpack-plugin')

import webpack from "webpack"

const babelLoader = {
  loader: 'babel-loader',
  options: {
    cacheDirectory: false,
    presets: ['@babel/preset-env']
  }
}




let web = {
  name: "web",
  entry: './src/index.ts',
  module: {
    rules: [
      {
        test: /\.js$/,
        use: [babelLoader]
      },
      {
        test: /\.tsx?$/,
        exclude: '/node_modules/',
        loader: 'ts-loader',
      },
      {
        test: /\.f7.html$/,
        use: [babelLoader, 'framework7-loader'],
      }
    ],
  },
  resolve: {
    extensions: ['*', '.js', '.jsx', '.tsx', '.ts'],

    fallback: {
      "util": require.resolve("util/"),
      "stream": require.resolve("stream-browserify"),
      "url": require.resolve("url/"),
      "path": require.resolve("path-browserify"),
      "assert": require.resolve("assert/")
    }
  },
  plugins: [
    new webpack.ProvidePlugin({
      process: 'process/browser',
    })
  ],
  output: {
    filename: 'index-browser.js',
    libraryTarget: 'umd',
    globalObject: 'this',
    path: path.resolve(__dirname, 'dist'),
  }
}



let node = {
  name: "node",
  entry: './src/index.ts',
  target: "node",
  externals: [nodeExternals()],
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        exclude: '/node_modules/',
        loader: 'ts-loader',
      },
      {
        test: /\.f7.html$/,
        use: [babelLoader, 'framework7-loader'],
      }
    ],
  },
  resolve: {
    extensions: ['*', '.js', '.jsx', '.tsx', '.ts']
  },
  output: {
    filename: 'index-node.js',
    libraryTarget: 'umd',
    path: path.resolve(__dirname, 'dist'),
  }
}



export {
  web,
  node
}


