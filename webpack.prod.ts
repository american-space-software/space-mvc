import { merge } from 'webpack-merge'
import { web, node } from './webpack.common'

let config = {
    mode: 'production',
    watch: true
}

//@ts-ignore
let mergedWeb = merge(web, config)

//@ts-ignore
let mergedNode = merge(node, config)

export default [mergedWeb, mergedNode]